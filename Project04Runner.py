#******************************************************************************
# CSCE 274 - Section 001 - Group 04 - Project 02
# Group Members: Amani Khawaja, Judson James, Will Carpenter
#
# Purpose of this Script: Completes the "Task 02" of Project 04
#******************************************************************************

# Import statements
import SerialInterface
import time
import struct
import threading
import sys
import random


# Constructors for Classes that will be used.
interface = SerialInterface.SerialInterface();

# Global Variables to be shared inside the threads
# lock - threading componenet from the Python library
# button - button that is pressed by a user
# data - any data that is sent and used.
lock = threading.Lock()
button = 0
data = 0

#******************************************************************************
# Function dedicated to the first thread that provides the various movements
# of the robot.
def Movement():
    global data
    global button
    global lock

    while True:
        if (data == 1):
            lock.acquire()
            rightside = interface.getLBR()
            pd = interface.update(rightside)
            centerLeft = interface.getLBCL()
            centerRight = interface.getLBCR()

            if (centerLeft > 200 or centerRight > 145):
                interface.rotateCCW()

            elif (0 < pd < 230):
                interface.controlledDrive(pd, 200)

            elif (pd < 0):
                interface.controlledDrive(200, 200+pd)

            elif (pd > 237):
                interface.controlledDrive(pd-200, 200)

            # if the robot is in dock, then it should exit the program
            if (data == 2):
                lock.acquire()
                sys.exit()

            # End of thread
            lock.release()
#******************************************************************************
# Thread Function that is designed to check the Buttons
def CheckButtons():
    global data
    global button
    global lock
    lastButton = 0
    while True:
        lock.acquire()
        buttons = interface.getButtons()
        power = buttons[3]

        if (lastButton == '0' and power == '1'):
            if (button == 0):
                data = 1
                button = 1
            else:
                interface.controlledDrive(0, 0)
                data = 0
                button = 0

        # End the thread and tell that the power button is pressed
        lock.release()
        lastButton = power
#******************************************************************************
# Function that checks for whether the robot is within the distance of the
# dock. If so, it will check the IR and make sure it leads to the dock.
def Jitter():
    global data
    global button
    global lock

    while True:
        lock.acquire()
        right = interface.getInfraredRight()
        left = interface.getInfraredLeft()

        # Checks if the robot is in the forcefield of the dock
        if (data == 1 and (left == 161 and data != 3)
                or (data == 1 and (right == 161 and data != 3))):
            interface.stop()
            time.sleep(0.25)
            data = 3

        # Jitter until the robot is in the forcefield of the dock
        if (data == 3):
            if (right != 161):
                interface.rotateCW()
            if (left != 161):
                interface.rotateCCW()

        # End the thread and release it
        lock.release()
#******************************************************************************
# Checks for the dock
def Dock():
    global data
    global button
    global lock
    while True:
        lock.acquire()
        state = interface.chargingState()
        omni = interface.getInfraredOmni()
        left = interface.getInfraredLeft()
        right = interface.getInfraredRight()
        bumpLeft = interface.getBumperAndDropStatus()[2]
        bumpRight = interface.getBumperAndDropStatus()[3]
        options = {5, 0, 1}

        # Looking for various tested instances of what can happen
        if ((left == 172 and right == 168 and data not in options)
            or (left == 164 and right == 172 and data not in options)
            or (left == 168 and right == 168 and data not in options)):
            data = 0
            interface.controlledDrive(0, 0)
            interface.controlledDrive(100, 100)

        # If both bumpers are true, then turn around.
        if (bumpLeft == '1' and bumpRight == '1' and data != 1):
            interface.stop()
            time.sleep(.25)
            interface.controlledDrive(-100, -100)
            time.sleep(0.25)
            interface.stop()
            interface.controlledDrive(-50, -50)
            interface.controlledDrive(50, 50)
            data = 5

        # Rotate Counter Clockwise if the bumpLeft is true
        if (bumpLeft == '1' and data != 1):
            interface.stop()
            time.sleep(.25)
            interface.controlledDrive(-100, -100)
            velocity = random.randint(50, 75)
            interface.rotateManually(velocity, "ccw")
            data = 5

        # if the right side is true, rotate clockwise
        if (bumpRight == '1' and data != 1):
            interface.stop()
            time.sleep(0.25)
            interface.controlledDrive(-100, -100)
            velocity = random.randint(50, 175)
            interface.rotateManually(velocity, "cw")

        # The robot has found the station, end the program.
        if (state == 2):
            interface.stop()
            data = 0
            sys.exit()

        # End of function, release the thread
        lock.release()
#******************************************************************************
# Overall main functionality of the robot
time.sleep(1)
interface.start()
interface.full()

# Thread initialization
movement = threading.Thread(name = 'Movement', target = Movement)
buttonCheck = threading.Thread(name = 'CheckButtons', target = CheckButtons)
jitter = threading.Thread(name = 'Jitter', target = Jitter)
dock = threading.Thread(name = 'Dock', target = Dock)

# Thread Starting
movement.start()
buttonCheck.start()
jitter.start()
dock.start()
